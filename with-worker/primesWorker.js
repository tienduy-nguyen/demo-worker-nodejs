const { workerData, parentPort } = require('worker_threads');

function isPrime(n) {
  for (let i = 2; i <= Math.sqrt(n); ++i) if (n % i === 0) return false;
  return n > 1;
}

function nthPrime(n) {
  let iterator = 1;
  let count = n;
  let result = [];
  while (count > 0) {
    isPrime(iterator) && result.push(iterator) && count--;
    iterator++;
  }
  return result;
}

parentPort.postMessage(nthPrime(workerData.n));
