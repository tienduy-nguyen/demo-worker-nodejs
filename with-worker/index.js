const http = require('http');
const url = require('url');
const { Worker } = require('worker_threads');

const server = http.createServer((req, res) => {
  const { pathname, query } = url.parse(req.url, true);
  if (pathname === '/primes') {
    const worker = new Worker('./primesWorker.js', {
      workerData: { n: query.n || 0 },
    });
    worker.on('error', () => {
      res.statusCode = 500;
      res.write('Oops there was an error...');
      res.end();
    });
    let result;
    worker.on('message', (message) => result.message);

    worker.on('exit', () => {
      res.setHeader('ContentType', 'application/json');
      res.write(JSON.stringify(result));
      res.end();
    });
  }
});

server.listen(6006);
