const http = require('http');
const url = require('url');
const { isPrime, nthPrime } = require('./primes');

const server = http.createServer((req, res) => {
  const { pathname, query } = url.parse(req.url, true);
  if (pathname === '/primes') {
    const result = nthPrime(query.n || 0);
    res.setHeader('Content-Type', 'application/json');
    res.write(JSON.stringify(result));
    res.end();
  } else {
    res.statusCode = 404;
    res.write('Not found');
    res.end();
  }
});
server.listen(6006);
